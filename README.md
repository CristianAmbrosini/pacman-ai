[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

# Pacman Game with AI

> Pac-Man is an action maze chase video game; the player controls the eponymous character through an enclosed maze. The objective of the game is to eat all of the dots placed in the maze while avoiding four colored ghosts — Blinky (red), Pinky (pink), Inky (cyan), and Clyde (orange) — that pursue him. When Pac-Man eats all of the dots, the player advances to the next level. If Pac-Man makes contact with a ghost, he will lose a life; the game ends when all lives are lost. - Wikipedia

![BreadthFirstSearch](img/PacmanAI.gif)

## Artificial Intelligence

The game is developed in Unity2D and allow both a human and AI to play.
A python code allow Machine Learning AI intelligent Agent controlling Pac-Man to train inside the game environment.
The way the agent learns is through reinforcement learning, a relatively simple loop of:
- **Observation**: where the agent gather data from the environment.
- **Decision**: based on the data acquired.
- **Action**: the actualization of the decision.
- **Reward**: if it does the right action then it gets a reward.

The cicle then will loop continuously.

## How to install

The source code does not comprehend the virtual environment (venv) and all the packages are listed inside the "requirements.txt" file. Start by *downloading the latest Python version* and opening command shell.

* Navigate to the project path with: <br />
`cd {your-path]`

* Create your virtual environment: <br />
`python -m venv venv`

* Make a virtual environment active by running an activate script in the virtual environment's executables directory: <br />
`venv\Scripts\activate`

* Now that we are inside the virtual environment install all the required packages running: <br />
`pip install -r requirements.txt`

## Run AI Agent

To run the learning process simply: <br />
`mlagents-learn --force`

And then click the Play button inside Unity Environment to start the training.
Enjoy!

## License
MIT License

Copyright (c) [year] [fullname]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
