using UnityEngine;


[RequireComponent(typeof(SpriteRenderer))]
public class AnimatedSprite : MonoBehaviour {
    public SpriteRenderer SpriteRenderer { get; private set; }
    public Sprite[] Sprites;
    public float AnimationTime = 0.25f;
    public int AnimationFrame { get; private set; }
    public bool Loop = true;

    public void Awake() => SpriteRenderer = GetComponent<SpriteRenderer>();
    private void Start() => InvokeRepeating(nameof(AdvanceAnimation), AnimationTime, AnimationTime);

    private void AdvanceAnimation(){
        if (!SpriteRenderer.enabled)
            return;
            
        AnimationFrame++;
        if (AnimationFrame >= Sprites.Length && Loop)
            AnimationFrame = 0;
        
        if (AnimationFrame >= 0 && AnimationFrame < Sprites.Length)
            SpriteRenderer.sprite = Sprites[AnimationFrame];
    }

    public void Restart(){
        AnimationFrame = -1;
        AdvanceAnimation();
    }
}
