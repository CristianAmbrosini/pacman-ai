using UnityEngine;

[RequireComponent(typeof(Movement))]
[RequireComponent(typeof(GhostHome))]
[RequireComponent(typeof(GhostChase))]
[RequireComponent(typeof(GhostScatter))]
[RequireComponent(typeof(GhostFrightened))]
[RequireComponent(typeof(GhostEaten))]
public class Ghost : MonoBehaviour {
    
    public Movement Movement { get; private set; }
    public GhostHome Home { get; private set; }
    public GhostChase Chase { get; private set; }
    public GhostScatter Scatter { get; private set; }
    public GhostFrightened Frightened { get; private set; }
    public GhostEaten Eaten { get; private set; }

    public SpriteRenderer Body;
    public SpriteRenderer Eyes;
    public SpriteRenderer Blue;
    public SpriteRenderer White;

    public GhostBehavior InitialBehaviour;
    public Transform Target;

    public int Points = 200;

    private void Awake(){
        Movement = GetComponent<Movement>();
        Home = GetComponent<GhostHome>();
        Chase = GetComponent<GhostChase>();
        Scatter = GetComponent<GhostScatter>();
        Frightened = GetComponent<GhostFrightened>();
        Eaten = GetComponent<GhostEaten>(); 
    }

    private void Start() => ResetState();

    public void ResetState(){
        Movement.ResetState();
        gameObject.SetActive(true);

        Frightened.Disable();
        Eaten.Disable();
        Chase.Disable();
        Scatter.Enable();
        if (Home != InitialBehaviour)
            Home.Disable();
        if (InitialBehaviour != null)
            InitialBehaviour.Enable();
    }

    public void ResetUI() {
        Body.enabled = true;
        Eyes.enabled = true;
        Blue.enabled = false;
        White.enabled = false;
    }

    public void SetPosition(Vector3 position) {
        position.z = transform.position.z;
        transform.position = position;
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Pacman") && !Eaten.enabled){
            if (Frightened.enabled)
                FindObjectOfType<GameManager>().GhostEaten(this);
            else
                FindObjectOfType<GameManager>().PacmanEaten();
        }    
    } 
}
