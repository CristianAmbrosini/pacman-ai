using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class GhostScatter : GhostBehavior {

    private void OnDisable() => Ghost.Chase.Enable();
    
    private void OnTriggerEnter2D(Collider2D other) {
        Node node = other.GetComponent<Node>();
        
        if (node == null || !enabled || Ghost.Frightened.enabled || Ghost.Eaten.enabled)
            return;
        
        int index = Random.Range(0, node.AvailableDirections.Count);

        // Avoid returning back if possible
        if (node.AvailableDirections[index] == -Ghost.Movement.Direction && node.AvailableDirections.Count > 1){
            index++;
            if (index >= node.AvailableDirections.Count)
                index = 0;
        }

        Ghost.Movement.SetDirection(node.AvailableDirections[index]);
    }
}
