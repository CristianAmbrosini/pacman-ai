using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class GhostChase : GhostBehavior {
    private void OnDisable() => Ghost.Scatter.Enable();
    
    private void OnTriggerEnter2D(Collider2D other) {
        Node node = other.GetComponent<Node>();
        
        if (node == null || !enabled || Ghost.Frightened.enabled || Ghost.Eaten.enabled)
            return;

        List<Vector2> list = node.AvailableDirections.ToList();
        if (list.Count > 1)
            list.Remove(-Ghost.Movement.Direction);

        Ghost.Movement.SetDirection(BestDirectionToTarget(list));
    }

    private Vector2 BestDirectionToTarget(List<Vector2> availableDirections){
        Vector2 direction = Vector2.zero;
        float minDistance = float.MaxValue;
        foreach(Vector2 availableDirection in availableDirections){
            Vector3 newPosition = transform.position + new Vector3(availableDirection.x, availableDirection.y, 0f);
            float distance = (Ghost.Target.position - newPosition).sqrMagnitude;

            if (distance < minDistance){
                direction = availableDirection;
                minDistance = distance;
            }
        }
        return direction;
    }
}
