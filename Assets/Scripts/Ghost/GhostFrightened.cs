using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class GhostFrightened : GhostBehavior {

    private void OnEnable() {
        Ghost.Movement.SpeedMultiplier = 0.5f;
        
        Ghost.Body.enabled = false;
        Ghost.Eyes.enabled = false;
        Ghost.Blue.enabled = true;
        Ghost.White.enabled = false;
    }

    private void OnDisable() {
        Ghost.Movement.SpeedMultiplier = 1.0f;
        if (!Ghost.Eaten.enabled)
            Ghost.ResetUI();
    }

    public override void Enable(float duration) {
        base.Enable(duration);
        Invoke(nameof(IsFlashing), duration / 2.0f);
    }

    private void IsFlashing(){
        Ghost.Blue.enabled = false;
        Ghost.White.enabled = true;
        Ghost.White.GetComponent<AnimatedSprite>().Restart();
    }


    private void OnCollisionEnter2D(Collision2D collision) {
        if (enabled && collision.gameObject.layer == LayerMask.NameToLayer("Pacman")) {
            Ghost.Eaten.Enable();
            Ghost.Frightened.Disable();
        }
    }

    private void OnTriggerEnter2D(Collider2D other) {
        Node node = other.GetComponent<Node>();
        
        if (node == null || !enabled)
            return;

        Ghost.Movement.SetDirection(BestDirectionAwayFromTarget(node.AvailableDirections));
    }

    private Vector2 BestDirectionAwayFromTarget(List<Vector2> availableDirections){
        Vector2 direction = Vector2.zero;
        float maxDistance = float.MinValue;
        foreach(Vector2 availableDirection in availableDirections){
            Vector3 newPosition = transform.position + new Vector3(availableDirection.x, availableDirection.y, 0.0f);
            float distance = (Ghost.Target.position - newPosition).sqrMagnitude;

            if (distance > maxDistance){
                direction = availableDirection;
                maxDistance = distance;
            }
        }
        return direction;
    }
}
