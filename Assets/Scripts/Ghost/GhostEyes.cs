using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class GhostEyes : MonoBehaviour {
    public SpriteRenderer SpriteRenderer { get; private set; }
    public Movement Movement { get; private set; }

    public Sprite Up;
    public Sprite Down;
    public Sprite Left;
    public Sprite Right;

    private void Awake() {
        SpriteRenderer = GetComponent<SpriteRenderer>();
        Movement = GetComponentInParent<Movement>();
    }

    private void FixedUpdate() {
        if (Movement.Direction == Vector2.up)
            SpriteRenderer.sprite = Up;
        if (Movement.Direction == Vector2.down)
            SpriteRenderer.sprite = Down;
        if (Movement.Direction == Vector2.right)
            SpriteRenderer.sprite = Right;
        if (Movement.Direction == Vector2.left)
            SpriteRenderer.sprite = Left;
    }
}
