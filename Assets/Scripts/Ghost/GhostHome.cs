using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class GhostHome : GhostBehavior {
    public Transform Inside;
    public Transform Outside;
    private void OnEnable() => StopAllCoroutines();

    private void OnDisable() {
        if (gameObject.activeInHierarchy)
            StartCoroutine(ExitTransition());
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        if (!enabled || collision.gameObject.layer != LayerMask.NameToLayer("Obstacle"))
            return;
        Ghost.Movement.SetDirection(-Ghost.Movement.Direction);
    }

    private IEnumerator ExitTransition(){
        Ghost.Movement.SetDirection(Vector2.up, true);
        Ghost.Movement.Body.isKinematic = true;
        Ghost.Movement.enabled = false;

        Vector3 position = transform.position;
        float duration = 0.5f;
        float elapsed = 0.0f;

        while (elapsed < duration){
            Ghost.SetPosition(Vector3.Lerp(position, Inside.position, elapsed / duration));
            elapsed += Time.fixedDeltaTime;
            yield return null;
        }

        elapsed = 0.0f;
        while (elapsed < duration){
            Ghost.SetPosition(Vector3.Lerp(Inside.position, Outside.position, elapsed / duration));
            elapsed += Time.fixedDeltaTime;
            yield return null;
        }

        Ghost.Movement.SetDirection(new Vector2(Random.value < 0.5f ? -1f : 1f, 0f), true);
        Ghost.Movement.Body.isKinematic = false;
        Ghost.Movement.enabled = true;
    }
}
