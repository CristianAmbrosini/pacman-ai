using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class GhostEaten : GhostBehavior {

    public float GhostEatenSpeed = 2f;

    private bool IsArrived = false;
    private void FixedUpdate(){
        if (enabled && !IsArrived && 
            Mathf.Abs(transform.position.x - Ghost.Home.Outside.position.x) < 1f && 
            Mathf.Abs(transform.position.y - Ghost.Home.Outside.position.y) < 1f)
            StartCoroutine(ReturnHome());
    }

    private void OnEnable() {
        Ghost.Body.enabled = false;
        Ghost.Eyes.enabled = true;
        Ghost.Blue.enabled = false;
        Ghost.White.enabled = false;

        IsArrived = false;
        Ghost.Movement.SpeedMultiplier = GhostEatenSpeed;
        Ghost.gameObject.layer = LayerMask.NameToLayer("GhostEaten");

        StopAllCoroutines();
    }

    private void OnDisable() {
        Ghost.ResetUI();

        Ghost.gameObject.layer = LayerMask.NameToLayer("Ghost");
        Ghost.Movement.SpeedMultiplier = 1f;
        Ghost.Home.Enable(4 + Random.Range(1, 10));
    }

    IEnumerator ReturnHome(){
        IsArrived = true;
        Ghost.Movement.SetDirection(Vector2.down, true);
        Ghost.Movement.Body.isKinematic = true;
        Ghost.Movement.enabled = false;

        Ghost.SetPosition(Ghost.Home.Outside.position);

        float duration = 0.3f;
        float elapsed = 0.0f;
        while (elapsed < duration){
            Ghost.SetPosition(Vector3.Lerp(Ghost.Home.Outside.position, Ghost.Home.Inside.position, elapsed / duration));
            elapsed += Time.fixedDeltaTime;
            yield return null;
        }

        Ghost.Movement.SetDirection(new Vector2(Random.value < 0.5f ? -1.0f : 1.0f, 0.0f));
        Ghost.Movement.Body.isKinematic = false;
        Ghost.Movement.enabled = true;

        Disable();
    }

    private void OnTriggerEnter2D(Collider2D other) {
        Node node = other.GetComponent<Node>();

        if (node == null || !enabled || IsArrived)
            return;

        List<Vector2> list = node.AvailableDirections.ToList();
        if (list.Count > 0)
            list.Remove(-Ghost.Movement.Direction);

        Ghost.Movement.SetDirection(BestDirectionToHome(list));
    }

    private Vector2 BestDirectionToHome(List<Vector2> availableDirections){
        Vector2 direction = Vector2.zero;
        float minDistance = float.MaxValue;
        foreach(Vector2 availableDirection in availableDirections){
            Vector3 newPosition = transform.position + new Vector3(availableDirection.x, availableDirection.y, 0.0f);
            float distance = (Ghost.Home.Outside.position - newPosition).sqrMagnitude;
            if (distance < minDistance){
                direction = availableDirection;
                minDistance = distance;
            }
        }
        return direction;
    }
}
