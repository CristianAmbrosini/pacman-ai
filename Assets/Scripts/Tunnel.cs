using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class Tunnel : MonoBehaviour {
    public Transform Connection;
    private void OnTriggerEnter2D(Collider2D other){
        Vector3 position = other.transform.position;
        position.x = Connection.position.x;
        position.y = Connection.position.y;

        other.transform.position = position;
    }
}
