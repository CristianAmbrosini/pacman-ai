using UnityEngine;
using Unity.MLAgents;
using Unity.MLAgents.Actuators;
using Unity.MLAgents.Sensors;

[RequireComponent(typeof(Pacman))]
public class PacmanAgent : Agent {

    public Pacman Pacman { get; private set; }

    public Transform GhostBlinky;
    public Transform GhostInky;
    public Transform GhostPinky;
    public Transform GhostClyde;

    private void Awake() => Pacman = GetComponent<Pacman>();

    public override void CollectObservations(VectorSensor sensor) {
        // Add data of pac-man position
        sensor.AddObservation(transform.position);

        // Add data of the four ghost positions (potential upgrade: raycast observations through grid observations)
        sensor.AddObservation(GhostBlinky.position);
        sensor.AddObservation(GhostInky.position);
        sensor.AddObservation(GhostPinky.position);
        sensor.AddObservation(GhostClyde.position);
    }

    public override void OnActionReceived(ActionBuffers actions) {
        int movement = actions.DiscreteActions[0];
        if (movement == 0)
            Pacman.MoveUp();
        if (movement == 1)
            Pacman.MoveRight();
        if (movement == 2)
            Pacman.MoveDown();
        if (movement == 3)
            Pacman.MoveLeft();
    }

    public void Reward(float score) => SetReward(score);

    public void End() => EndEpisode();
}
