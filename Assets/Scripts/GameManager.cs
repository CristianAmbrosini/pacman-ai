using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {
    public Ghost[] Ghosts;
    public Pacman Pacman;
    public Transform Pellets;
    public Transform FloatingTextPrefab;

    public Text GameOverText;
    public Text ScoreText;
    public Text HighScoreText;
    public Text LivesText;

    public Image Life1;
    public Image Life2;
    public Image Life3;

    public PacmanAgent MLAgent;

    public int GhostMultiplier { get; private set; } = 1;
    public int Score { get; private set; }
    public int HighScore { get; private set; } = 0;
    public int Lives { get; private set; }

    private void Start() => NewGame();

    private void FixedUpdate(){
        if (Lives <= 0 && Input.anyKeyDown)
            NewGame();
    }

    private void NewGame(){
        SetScore(0);
        SetLives(3);
        NewRound();
    }

    private void NewRound(){
        GameOverText.enabled = false;

        foreach(Transform pellet in Pellets)
            pellet.gameObject.SetActive(true);
        
        ResetState();
    }

    private void ResetState(){
        for(int i = 0; i < Ghosts.Length; i++)
            Ghosts[i].ResetState();
        ResetGhostMultiplier();

        Pacman.ResetState();
    }

    private void GameOver(){
        GameOverText.enabled = true;

        for (int i = 0; i < Ghosts.Length; i++)
            Ghosts[i].gameObject.SetActive(false);

        Pacman.gameObject.SetActive(false);
    }

    private void SetScore(int score){
        Score = score;
        MLAgent.Reward(score);
        ScoreText.text = Score.ToString().PadLeft(2, '0');
        if (score > HighScore) {
            HighScore = score;
            HighScoreText.text = HighScore.ToString().PadLeft(2, '0');
        }
    }

    private void SetLives(int lives) {
        Lives = lives;
        LivesText.text = Lives.ToString();
        if (lives >= 3) {
            Life1.enabled = true;
            Life2.enabled = true;
            Life3.enabled = true;
        }
        else if(lives == 2) {
            Life1.enabled = true;
            Life2.enabled = true;
            Life3.enabled = false;
        }
        else if (lives == 1) {
            Life1.enabled = true;
            Life2.enabled = false;
            Life3.enabled = false;
        }
        else {
            Life1.enabled = false;
            Life2.enabled = false;
            Life3.enabled = false;
        }
    }

    public void SpawnFloatingPoints(Vector3 position, string text) {
        Instantiate(FloatingTextPrefab, position, Quaternion.identity);
        FloatingTextPrefab.GetComponentInChildren<TextMesh>().text = text;
    }

    public void GhostEaten(Ghost ghost){
        SetScore(Score + (ghost.Points * GhostMultiplier));
        SpawnFloatingPoints(ghost.transform.position, (ghost.Points * GhostMultiplier).ToString());
        GhostMultiplier++;
    }

    public void PacmanEaten(){
        // AI
        if (MLAgent.enabled) {
            MLAgent.Reward(-1f);
            MLAgent.End();
            NewGame();
        }
        // Human
        else {
            Pacman.DeathSequenceStart();
            SetLives(Lives - 1);
            if (Lives > 0)
                Invoke(nameof(ResetState), 3f);
            else
                GameOver();
        }
    }

    public void PelletEaten(Pellet pellet){
        pellet.gameObject.SetActive(false);
        SetScore(Score + pellet.Points);
        if (!HasRemaninengPellets()){
            Pacman.gameObject.SetActive(false);
            Invoke(nameof(NewRound), 3f);
        }
    }

    public void PowerPelletEaten(PowerPellet pellet){
        for(int i = 0; i < Ghosts.Length; i++)
            Ghosts[i].Frightened.Enable(pellet.Duration);
        
        PelletEaten(pellet);
        CancelInvoke();
        Invoke(nameof(ResetGhostMultiplier), pellet.Duration);
    }

    private bool HasRemaninengPellets(){
        foreach(Transform pellet in Pellets)
            if (pellet.gameObject.activeSelf)
                return true;
        return false;
    }

    private void ResetGhostMultiplier() => GhostMultiplier = 1;

}
