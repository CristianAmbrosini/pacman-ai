using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(Collider2D))]
[RequireComponent(typeof(Movement))]
public class Pacman : MonoBehaviour {
    public SpriteRenderer SpriteRenderer { get; private set; }
    public Collider2D Collider { get; private set; }
    public Movement Movement { get; private set; }

    public AnimatedSprite DeathSequence;

    private void Awake() {
        SpriteRenderer = GetComponent<SpriteRenderer>();
        Collider = GetComponent<Collider2D>();
        Movement = GetComponent<Movement>();
    }

    private void FixedUpdate(){
        if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow))
            MoveUp();
        if (Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
            MoveDown();
        if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow))
            MoveRight();
        if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow))
            MoveLeft();
        RotateSprite();
    }

    public void MoveUp() => Movement.SetDirection(Vector2.up);
    public void MoveRight() => Movement.SetDirection(Vector2.right);
    public void MoveDown() => Movement.SetDirection(Vector2.down);
    public void MoveLeft() => Movement.SetDirection(Vector2.left);

    private void RotateSprite(){
        float angle = Mathf.Atan2(Movement.Direction.y, Movement.Direction.x);
        transform.rotation = Quaternion.AngleAxis(angle * Mathf.Rad2Deg, Vector3.forward);
    }

    public void ResetState(){
        enabled = true;
        SpriteRenderer.enabled = true;
        Collider.enabled = true;
        DeathSequence.enabled = false;
        DeathSequence.SpriteRenderer.enabled = false;
        Movement.ResetState();
        gameObject.SetActive(true);
    }

    public void DeathSequenceStart() {
        enabled = false;
        SpriteRenderer.enabled = false;
        Collider.enabled = false;
        Movement.enabled = false;
        DeathSequence.enabled = true;
        DeathSequence.SpriteRenderer.enabled = true;
        DeathSequence.Restart();
    }
}
