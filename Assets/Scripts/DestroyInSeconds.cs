using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyInSeconds : MonoBehaviour {
    public float Seconds;

    private void Start() => Destroy(gameObject, Seconds);
}
