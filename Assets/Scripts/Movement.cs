using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Movement : MonoBehaviour {
    public float Speed = 8f;
    public float SpeedMultiplier = 1f;
    public Vector2 InitialDirection;
    public LayerMask ObstacleLayer;

    public Rigidbody2D Body { get; private set; }
    public Vector2 Direction { get; private set; }
    public Vector2 NextDirection { get; private set; }
    public Vector3 StartingPosition { get; private set; }

    private void Awake() {
        Body = GetComponent<Rigidbody2D>();
        StartingPosition = transform.position;
    }

    private void Start() => ResetState();
    public void ResetState() {
        SpeedMultiplier = 1f;
        Direction = InitialDirection;
        NextDirection = Vector2.zero;
        Body.isKinematic = false;
        transform.position = StartingPosition;
        enabled = true;
    }

    private void Update() {
        if (NextDirection != Vector2.zero)
            SetDirection(NextDirection);
    }

    private void FixedUpdate() {
        Vector2 position = Body.position;
        Vector2 translation = Speed * SpeedMultiplier * Time.fixedDeltaTime * Direction;

        Body.MovePosition(position + translation);
    }

    public void SetDirection(Vector2 direction, bool forced = false) {
        if (forced || !Occupied(direction)) {
            Direction = direction;
            NextDirection = Vector2.zero;
        }
        else
            NextDirection = direction;
    }

    public bool Occupied(Vector2 direction) {
        RaycastHit2D hit = Physics2D.BoxCast(transform.position, Vector2.one * 0.75f, 0f, direction, 1.5f, ObstacleLayer);
        return hit.collider != null;
    }
}
